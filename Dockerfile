FROM tomcat 
WORKDIR webapps 
COPY target/*.war .
RUN rm -rf ROOT && mv *.war ROOT.war
ENTRYPOINT ["sh", "/opt/tomcat/apache-tomcat-9.0.37/bin/startup.sh"]
